module.exports = {
  callback: function (req, res) {
    var action = req.param('action');

    function negotiateError(err) {
      if (action === 'register') {
        res.redirect('/register');
      }
      else if (action === 'login') {
        res.redirect('/login');
      }
      else if (action === 'disconnect') {
        res.redirect('back');
      }
      else {
        // make sure the server always returns a response to the client
        // i.e passport-local bad username/email or password
        res.send(403, err);
      }
    }

    sails.services.passport.callback(req, res, function (err, user, info, status) {
      if (err || !user) {
        sails.log.warn(user, err, info, status);
        if (!err && info) {
          return negotiateError(info);
        }
        return negotiateError(err);
      }

      req.login(user, function (err) {
        if (err) {
          sails.log.warn(err);
          return negotiateError(err);
        }

        req.session.authenticated = true;

        // Upon successful login, optionally redirect the user if there is a
        // `next` query param
        if (req.query.next) {
          var url = sails.services.authservice.buildCallbackNextUrl(req);
          return res.status(302).set('Location', url);
        }

        // Same
        if (req.session.returnTo) {
          return res.redirect(req.session.returnTo);
        }

        sails.log.info('user', user, 'authenticated successfully');
        return res.redirect('/');
      });
    });
  }
}