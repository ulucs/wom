/**
 * SwarmCheckinsController
 *
 * @description :: Server-side logic for managing Swarmcheckins
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  pushHandler(req, res) {
    const response = req.body;
    SwarmService.receive(response);
    return res.ok();
  },

  realtime(req, res) {
    if (!req.isSocket) {
      return res.badRequest('You have to connect using sockets!');
    }

    sails.sockets.join(req.socket, `swarms-user${req.user.id}`);
    return res.ok();
  }
};
