/**
 * TweetsController
 *
 * @description :: Server-side logic for managing Tweets
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	realtime(req, res) {
    if (!req.isSocket) {
      return res.badRequest('You have to connect using sockets!');
    }

    if(!TwitterService.online()) {
      TwitterService.initialize(Subscriptions, Tweets);
    }
    sails.sockets.join(req.socket, `tweets-user${req.user.id}`);
    return res.ok();
  }
};

