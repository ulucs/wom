/**
 * SubscriptionsController
 *
 * @description :: Server-side logic for managing subscriptions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	
  updateSwarmVenues(req, res) {
    const {token} = req.body;
    SwarmService.updateVenues(token, req.user)
      .then(res.json)
      .catch(res.negotiate);
  },
};

