/**
 * sessionAuth
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
const login = require('connect-ensure-login');

module.exports = (req, res, next) => {
  req.isAuthenticated = () => req.session.authenticated;
  login.ensureLoggedIn('/login')(req, res, next);
};