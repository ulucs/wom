/**
 * SwarmCheckins.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    userId: 'string',
    username: 'string',
    userPhoto: 'string',
    venueId: 'string',
    venuename: 'string',
    checkinNo: 'integer'
  },

  beforeCreate(values, cb) {
    const {userId, owner, createdBy} = values;
    SwarmUser.findOrCreate({userId, owner, createdBy})
      .then(({checkinCount = 0}) => {
        values.checkinNo = checkinCount + 1;
        cb();
      })
      .catch(cb);
  },
  afterCreate(checkin, cb) {
    const {userId, owner, createdBy, username, userPhoto, checkinNo} = checkin;
    const lastActivity = checkin.createdAt;
    const checkinCount = checkinNo;
    sails.sockets.broadcast(`swarms-user${owner}`, 'new_swarm', checkin);
    SwarmUser.update({userId, owner, createdBy}, {username, userPhoto, lastActivity, checkinCount})
      .then(() => cb())
      .catch(cb);
  }
};
