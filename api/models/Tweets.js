/**
 * Tweets.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    username: 'string',
    screenName: 'string',
    photo: 'string',
    text: 'string',
    tweetNo: 'integer'
  },

  beforeCreate(values, cb) {
    const {screenName, owner, createdBy} = values;
    TwitterUser.findOrCreate({screenName, owner, createdBy})
      .then(({tweetCount = 0}) => {
        values.tweetNo = tweetCount + 1;
        cb();
      })
      .catch(cb);
  },
  afterCreate(entry, cb) {
    sails.sockets.broadcast(`tweets-user${entry.owner}`, 'new_tweet', entry);
    const {screenName, username, photo, owner, createdBy, tweetNo} = entry;
    const tweetCount = tweetNo;
    const lastActivity = entry.createdAt;
    TwitterUser.update({screenName, owner, createdBy}, {username, photo, lastActivity, tweetCount})
      .then(() => cb())
      .catch(cb);
  }
};
