/**
 * Subscriptions.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    service: 'string',
    active: 'boolean',
    filter: 'string'
  },

  afterCreate(model, cb) {
    TwitterService.initialize(Subscriptions, Tweets);
    cb();
  },

  afterUpdate(model, cb) {
    TwitterService.initialize(Subscriptions, Tweets);
    cb();
  },

  afterDestroy(models, cb) {
    TwitterService.initialize(Subscriptions, Tweets);
    cb();
  }
};

