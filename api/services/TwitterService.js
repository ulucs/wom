const Twitter = require('twitter');
const client = new Twitter(sails.config.twitter);

let doRestart = false;

let stream;

function attachOwners(tweet, subs) {
  return subs
    .filter(({filter}) => tweet.text.toLowerCase().includes(filter.toLowerCase()))
    .map(({owner, createdBy}) => Object.assign({owner, createdBy}, tweet));
}

function reduceToUseful(tweet) {
  const {name, screen_name, profile_image_url_https} = tweet.user;
  const text = tweet.extended_tweet ? tweet.extended_tweet.full_text : tweet.text;
  return {
    username: name,
    screenName: screen_name,
    photo: profile_image_url_https,
    text
  };
}

function createAndAttach(subs) {
  const tags = subs.map(i => i.filter).join(',');
  stream = client.stream('statuses/filter', {track: tags});
  console.log(`Restarted stream with tags: ${tags}`);
  stream.on('error', error => {
    console.error(error);
    stream.destroy();
    doRestart = true;
  });
  stream.on('data', event => {
    const tweet = reduceToUseful(event);
    Promise.all(attachOwners(tweet, subs).map(twt => Tweets.create(twt))).then(a => a);
  });
}

setInterval(function restartStream() {
  if (!doRestart) {
    return;
  }
  doRestart = false;
  if (stream) {
    stream.destroy();
    stream = null;
  }
  Subscriptions.findByService('twitter').then(subs => {
    if (subs.length === 0) {
      return;
    }
    createAndAttach(subs);
  });
}, 20 * 1000);

module.exports = {
  initialize() {
    doRestart = true;
  },
  online() {
    return Boolean(stream);
  }
};
