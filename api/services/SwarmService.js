const rp = require('request-promise');

function isCheckin(resp) {
  return Object.keys(resp).includes('checkin');
}

async function reduceToUseful(checkin) {
  const {user, venue} = JSON.parse(checkin);
  const userId = user.id;
  const username = `${user.firstName || ''} ${user.lastName || ''}`.trim();
  const imgres = '256x256';
  const {photo} = user;
  const userPhoto = `${photo.prefix}${imgres}${photo.suffix}`;

  const venueId = venue.id;
  const venuename = venue.name;

  const {owner = 'orphan', createdBy = 'orphan'} = (await SwarmVenues.findOne({
    swarmId: venueId
  })) || {};

  return {
    userId,
    username,
    userPhoto,
    venueId,
    venuename,
    owner,
    createdBy
  };
}

module.exports = {
  async receive(response) {
    if (isCheckin(response))
      try {
        const checkin = await reduceToUseful(response.checkin);
        SwarmCheckins.create(checkin).then(a => a);
      } catch (err) {
        console.error(err);
      }
  },
  async updateVenues(token, user) {
    const {owner, createdBy} = user;
    const venues = await rp(
      `https://api.foursquare.com/v2/venues/managed?oauth_token=${token}&v=20170418`
    )
      .then(JSON.parse)
      .then(resp => resp.response.venues.items);
    const deleted = await SwarmVenues.destroy({owner, createdBy});
    const userVenues = await Promise.all(
      venues.map(async venue => {
        const {id, name} = venue;
        const created = await SwarmVenues.create({
          owner,
          createdBy,
          name,
          swarmId: id
        });
        return created;
      })
    );
    return userVenues;
  }
};
