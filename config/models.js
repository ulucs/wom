/**
 * Default model configuration
 * (sails.config.models)
 *
 * Unless you override them, the following properties will be included
 * in each of your models.
 *
 * For more info on Sails models, see:
 * http://sailsjs.org/#!/documentation/concepts/ORM
 */

module.exports.models = {
  grants: {
    admin: [
      {
        action: 'create'
      },
      {
        action: 'read'
      },
      {
        action: 'update'
      },
      {
        action: 'delete'
      }
    ],
    registered: [
      {
        action: 'create'
      },
      {
        action: 'read',
        relation: 'owner'
      },
      {
        action: 'update',
        relation: 'owner'
      },
      {
        action: 'delete',
        relation: 'owner'
      }
    ],
    public: []
  },
  /***************************************************************************
  *                                                                          *
  * Your app's default connection. i.e. the name of one of your app's        *
  * connections (see `config/connections.js`)                                *
  *                                                                          *
  ***************************************************************************/
  connection: 'mongo',

  /***************************************************************************
  *                                                                          *
  * How and whether Sails will attempt to automatically rebuild the          *
  * tables/collections/etc. in your schema.                                  *
  *                                                                          *
  * See http://sailsjs.org/#!/documentation/concepts/ORM/model-settings.html  *
  *                                                                          *
  ***************************************************************************/
  migrate: 'alter'
};
