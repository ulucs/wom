angular.module('womWorks')
  .filter('fromNow', ($interval) => {
    $interval(function (){}, 30 * 1000);

    function fromNow(date) {
      if (!date) {
        return 'Never';
      }
      return moment(date).fromNow();
    }
    fromNow.$stateful = true;

    return fromNow;
  });
