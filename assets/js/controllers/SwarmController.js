angular
  .module('womWorks')
  .controller('SwarmController', function(
    sails,
    TokenService,
    Query,
    recentCheckins,
    managedVenues
  ) {
    const sc = this;
    sc.managedVenues = managedVenues;
    sc.feed = recentCheckins;
    sc.userFilter = '';
    sc.isUpdating = false;

    if (TokenService.hasToken) {
      const token = TokenService.oauth;
      TokenService.oauth = '';
      sc.isUpdating = true;
      sails.post('/subscriptions/updateSwarmVenues', {token}, () => sc.getVenues());
    }

    sc.getVenues = () =>
      sails.get('/swarmvenues', data => {
        sc.managedVenues = data;
        sc.isUpdating = false;
      });

    sc.giveReward = (userId, username) => {
      sails.post(`/swarmReward`, {userId}, data => {
        sc.userFilter = username;
        sc.getUsers();
      });
    };

    sc.searchUser = username => {
      sc.userFilter = username;
      sc.getUsers();
    };

    sc.getUsers = () => {
      sc.users = [];
      sc.gettingUsers = true;
      const q = new Query({username: {startsWith: sc.userFilter}});
      sails.get(`/swarmUser?where=${q}`, data => {
        sc.userFilter = '';
        sc.users = data;
        sc.gettingUsers = false;
      });
    };

    sails.get('/swarmCheckins/realtime', (data, jwr) => {
      sails.on('new_swarm', entry => {
        sc.feed.unshift(entry);
      });
    });
  });
