angular
  .module('womWorks')
  .controller('SwarmWelcomeController', function(
    WelcomeScreen,
    sails,
    $timeout,
    Fullscreen
  ) {
    const wsc = this;
    const welcomeScreen = new WelcomeScreen(
      {src: '/images/example1.jpg'},
      {src: '/images/example2.jpg'}
    );
    const fileInput = document.querySelector('#bgInput');

    wsc.inceUnlu = name => /[eiöü][^eiöü]*/.test(name);

    fileInput.onchange = event =>
      $timeout(() => {
        if (event.target.files[0]) {
          Array.from(event.target.files)
            .map(file => window.URL.createObjectURL(file))
            .forEach(src => wsc.bgList.push({src}));

          event.target.value = '';
        }
      });

    wsc.testCheckin = () => {
      const randNames = ['Ahmet', 'Mehmet', 'Fulya', 'Batı', 'Naciye', 'Esen'];
      const userPhoto = 'https://igx.4sqi.net/img/user/256x256/blank_girl.png';
      // pick random element
      const [username] = randNames.splice(
        Math.floor(Math.random() * randNames.length),
        1
      );
      welcomeScreen.addPerson({username, userPhoto});
    };

    wsc.bgList = welcomeScreen.bgList;
    wsc.screen = welcomeScreen;
    wsc.goFullscreen = () => Fullscreen.enable(document.querySelector('#welcomeScreen'));

    sails.get('/swarmCheckins/realtime', (data, jwr) => {
      sails.on('new_swarm', entry => {
        welcomeScreen.addPerson(entry);
      });
    });
  });
