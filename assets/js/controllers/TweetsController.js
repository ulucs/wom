angular
  .module('womWorks')
  .controller('TweetsController', function($timeout, sails, Query, recentTweets, subs) {
    function getSubs() {
      const q = new Query({service: 'twitter'});
      sails.get(`/subscriptions?where=${q}`, data => {
        tw.tags = data;
      });
    }

    const tw = this;
    tw.feed = recentTweets;
    tw.tags = subs;
    tw.userFilter = '';

    tw.addSubscription = () =>
      sails.post(
        '/subscriptions',
        {
          service: 'twitter',
          filter: tw.newFilter
        },
        () => {
          tw.newFilter = '';
          getSubs();
        }
      );

    tw.removeSubscription = id => {
      sails.delete(`/subscriptions/${id}`, () => {
        getSubs();
      });
    };

    tw.giveReward = screenName => {
      sails.post(
        `/twitterReward`,
        {
          screenName
        },
        data => {
          tw.userFilter = data.screenName;
          tw.getUsers();
        }
      );
    };

    tw.searchUser = username => {
      tw.userFilter = username;
      tw.getUsers();
    };

    tw.getUsers = () => {
      tw.users = [];
      tw.gettingUsers = true;
      const q = new Query({screenName: {startsWith: tw.userFilter}});
      sails.get(`/twitterUser?where=${q}`, data => {
        tw.userFilter = '';
        tw.users = data;
        tw.gettingUsers = false;
      });
    };

    sails.get('/tweets/realtime', (data, jwr) => {
      sails.on('new_tweet', entry => {
        tw.feed.unshift(entry);
      });
    });
  });
