angular.module('womWorks').controller('NavController', function($location, TokenService) {
  const nc = this;
  nc.active = () => $location.path();

  const token = ($location.hash().match(/access_token=([A-Z0-9]+)/) || [])[1];
  if (token) {
    TokenService.oauth = token;
  }
});
