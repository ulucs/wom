angular
  .module('womWorks', ['ui.router', 'FBAngular'])
  .config(function($urlRouterProvider, $stateProvider) {
    $stateProvider
      .state('twitter', {
        url: '/twitter',
        templateUrl: '../templates/tweets.html',
        controller: 'TweetsController',
        controllerAs: 'tweets',
        resolve: {
          recentTweets(sails) {
            return new Promise((resolve, reject) =>
              sails.get('/tweets?sort=createdAt DESC', resolve)
            );
          },
          subs(sails) {
            return new Promise((resolve, reject) =>
              sails.get('/subscriptions?service=twitter', resolve)
            );
          }
        }
      })
      .state('swarm', {
        url: '/swarm',
        templateUrl: '../templates/swarm.html',
        controller: 'SwarmController',
        controllerAs: 'swarm',
        resolve: {
          recentCheckins(sails) {
            return new Promise((resolve, reject) =>
              sails.get('/swarmcheckins?sort=createdAt DESC', resolve)
            );
          },
          managedVenues(sails) {
            return new Promise((resolve, reject) => sails.get('/swarmvenues', resolve));
          }
        }
      })
      .state('swarmWelcome', {
        url: '/swarmwelcome',
        templateUrl: '../templates/welcome.html',
        controller: 'SwarmWelcomeController',
        controllerAs: 'wsc'
      });
    $urlRouterProvider.otherwise('/swarm');
  })
  .config(function($compileProvider) {
    $compileProvider.debugInfoEnabled(true);
    localforage.setDriver(localforage.INDEXEDDB);
  });
