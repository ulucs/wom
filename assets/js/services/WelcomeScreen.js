angular.module('womWorks').factory('WelcomeScreen', function($interval, SortableList) {
  function dataToObject(src) {
    return fetch(src).then(resp => resp.blob()).then(blob => ({
      src: URL.createObjectURL(blob)
    }));
  }
  function objectToData({src}) {
    return new Promise(resolve => {
      const reader = new FileReader();
      reader.onload = event => resolve(reader.result);
      fetch(src).then(resp => resp.blob()).then(blob => reader.readAsDataURL(blob));
    });
  }
  class WelcomeScreen {
    constructor(...items) {
      this._welcomeDuration = 10 * 1000;
      this._welcomeList = [];
      this._bgInterval = 10 * 1000;
      this._bgCounter = 0;

      this._bgList = new SortableList();
      this.bgList = new Proxy(this._bgList, {
        get: (handler, target) => {
          if (typeof handler[target] === 'function')
            return (...args) => {
              const temp = handler[target](...args);
              this.save();
              return temp;
            };
          return handler[target];
        }
      });

      this.load().then(oldItems => {
        const initialItems = oldItems.length === 0 ? items : oldItems;
        this._bgList.push(...initialItems);
        this.activateBg();
      });
    }
    set welcomeDuration(time) {
      this._welcomeDuration = time * 1000;
      return time;
    }
    set bgInterval(time) {
      this._bgInterval = time * 1000;
      return time;
    }
    get current() {
      return this._welcomeList[0];
    }
    get bgImage() {
      return this._bgList[this._bgCounter];
    }
    save() {
      Promise.all(this._bgList.map(objectToData))
        .then(data => localforage.setItem('showPictures', data))
        .then(() => console.info('Images saved successfully'));
    }
    load() {
      return localforage
        .getItem('showPictures')
        .then(items => items || [])
        .then(items => Promise.all(items.map(dataToObject)));
    }
    addPerson(viewObj) {
      this._welcomeList.push(viewObj);
      if (this._welcomeList.length === 1) {
        this.activate();
        $interval.cancel(this.bgTimer);
      }
    }
    activateBg() {
      this.bgTimer = $interval(() => {
        // let's prevent stuff from getting divided by zero
        this._bgCounter = (this._bgCounter + 1) % (this._bgList.length || 1);
      }, this._bgInterval);
    }
    activate() {
      this.timer = $interval(() => {
        this._welcomeList.shift();
        if (this._welcomeList.length === 0) {
          $interval.cancel(this.timer);
          this.activateBg();
        }
      }, this._welcomeDuration);
    }
  }
  return WelcomeScreen;
});
