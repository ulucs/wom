angular.module('womWorks').factory('Query', function() {
  class Query {
    constructor(qs) {
      Object.assign(this, qs);
    }
    toString() {
      return encodeURIComponent(JSON.stringify(this));
    }
  }
  return Query;
});
