angular.module('womWorks')
  .factory('sails', function($timeout) {
    const sails = {
      get(route, cb) {
        return io.socket.get(route, resp => 
          $timeout(() => cb(resp))
        );
      },
      post(route, data, cb) {
        return io.socket.post(route, data, resp => 
          $timeout(() => cb(resp))
        );
      },
      delete(route, cb) {
        return io.socket.delete(route, resp => 
          $timeout(() => cb(resp))
        );
      },
      put(route, data, cb) {
        return io.socket.put(route, data, resp => 
          $timeout(() => cb(resp))
        );
      },
      on(event, cb) {
        return io.socket.on(event, resp =>
          $timeout(() => cb(resp))
        )
      }
    };
    return sails;
  });
