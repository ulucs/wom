angular.module('womWorks').factory('TokenService', function() {
  class TokenService {
    get oauth() {
      return this.token;
    }
    set oauth(value) {
      this.token = value;
      return this.token;
    }
    get hasToken() {
      return Boolean(this.token);
    }
  }
  const tokenS = new TokenService();
  return tokenS;
});
