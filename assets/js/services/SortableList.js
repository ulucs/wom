angular.module('womWorks').factory('SortableList', function() {
  const sortableFuncs = {
    // class SortableList extends Array {
    swap(i1, i2) {
      const tmp = this[i1];
      this[i1] = this[i2];
      this[i2] = tmp;
    },
    toStart(i) {
      const [item] = this.splice(i, 1);
      this.unshift(item);
    },
    toEnd(i) {
      const [item] = this.splice(i, 1);
      this.push(item);
    },
    remove(i) {
      this.splice(i, 1);
    }
  };
  // looks like the real asshole is babel here
  return function SortableListConstructor(...items) {
    const dataArr = items;
    Object.assign(dataArr, sortableFuncs);
    return dataArr;
  };
  // return SortableList;
});
